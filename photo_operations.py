from photo_average_color import average_color
from photo_deleter import delete_images
from photo_mover import move_images
from photo_oraginiser import organise_folder


def main():
    supported_operations = ['organise', 'O', 'delete', 'D', 'move', 'M', 'average', 'A']
    print('Supported operations are: ' + str(supported_operations))

    operation = input('Operation: ')

    if operation in supported_operations:
        root = input('Image Path: ' if (operation == 'A' or operation == 'average') else 'Root Directory: ')

        if operation == 'delete' or operation == 'D':
            delete_images(root)
        
        if operation == 'move' or operation == 'M':
            move_images(root)
        
        if operation == 'average' or operation == 'A':
            average_color(root)
        
        if operation == 'organise' or operation == 'O':
            organise_folder(root)

        input('\n - Press a key to exit - ')
    else:
        print('\nOperation not supported!')
        main()


main()
