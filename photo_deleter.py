from os import walk, remove, path


def delete_images(path_to_images):
    deleted_images = 0

    for root, dirs, files in walk(path_to_images):
        if root is path_to_images:
            for file in files:
                if (".CR2" in file or ".CR3" in file) and not path.exists(root + '\\' + switch_extension(file)):
                    remove(root + '\\' + file)
                    deleted_images += 1

    print(str(deleted_images) + ' Images Deleted!')


def switch_extension(image):
    return image.replace(".CR2", ".JPG").replace(".CR3", ".JPG")
