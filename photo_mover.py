from os import path, makedirs, walk
from shutil import move


def move_images(path_to_images):
    prefix = input("Prefix (MA_IMG_): ") or 'MA_IMG_'
    extension = input('Extension (.CR3): ') or '.CR3'
    selected_image_ids = map(complete_image_id, input('Selected Images: ').split())

    moved_images = 0

    for root, dirs, files in walk(path_to_images):
        if root is path_to_images:
            destination_directory_path = root + '\\Edited\\Raw'

            if not path.exists(destination_directory_path):
                makedirs(destination_directory_path)

            for selected_image_id in selected_image_ids:
                image_name = prefix + selected_image_id + extension

                if image_name in files:
                    src = root + '\\' + image_name
                    destination = destination_directory_path + '\\' + image_name

                    move(src, destination)
                    moved_images += 1
                else:
                    print(image_name + ' can not be found')

    print(str(moved_images) + ' Images Moved!')


def complete_image_id(image_id):
    completed_image_id = image_id

    if len(image_id) == 1:
        completed_image_id = "000" + image_id
    elif len(image_id) == 2:
        completed_image_id = "00" + image_id
    elif len(image_id) == 3:
        completed_image_id = "0" + image_id

    return completed_image_id
