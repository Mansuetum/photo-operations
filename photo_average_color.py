from PIL import Image


def average_color(image_path):
    image = Image.open(image_path)
    rgb_image = image.convert('RGB')
    (width, height) = image.size
    total_pixels = width * height
    red = 0
    green = 0
    blue = 0

    for width_index in range(width):
        for height_index in range(height):
            (r, g, b) = rgb_image.getpixel((width_index, height_index))
            red += r
            green += g
            blue += b

    average_red = int(red / total_pixels)
    average_green = int(green / total_pixels)
    average_blue = int(blue / total_pixels)
    average_rgb = 'rgb(' + str(average_red) + ', ' + str(average_green) + ', ' + str(average_blue) + ')'
    print('The average color is: ', average_rgb)
