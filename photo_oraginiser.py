import concurrent.futures
import os

import exifread

from photo_mover import complete_image_id

JPG = 'JPG'
jpg = 'jpg'
CR2 = 'CR2'
CR3 = 'CR3'
XMP = 'xmp'
AFPHOTO = 'afphoto'

supported_extensions = [JPG, jpg]
excluded_dirs = ['Edited', 'Canon Ready', 'Digital Art', 'Photoshop']
prefix = ''
use_only_exif = False
completed_subdirectories = 0


def organise_folder(path):
    global prefix, use_only_exif, completed_subdirectories
    use_only_exif = input('Use only exif for filename (T?): ') == 'T'

    if not use_only_exif:
        prefix = input("Prefix (MA_IMG_): ") or 'MA_IMG_'

    check_folder(path)
    print("Root organised!")

    filtered_dirs = []

    for root, dirs, _ in os.walk(path):
        for directory in dirs:
            if not any(excluded in directory or excluded in root for excluded in excluded_dirs):
                filtered_dirs.append((root, directory))

    for root, directory in filtered_dirs:
        check_folder(os.path.join(root, directory))
        completed_subdirectories += 1
        print("\nSubdirectory %s/%s complete!\n" %
              (str(completed_subdirectories), str(len(filtered_dirs))))


def check_folder(root):
    global use_only_exif

    iterate_files(root, rename_to_exif, True)

    if not use_only_exif:
        iterate_files(root, rename_to_indexed, False)


def iterate_files(root, callback, show_summary):
    dirs = os.listdir(root)
    files = [f for f in dirs if os.path.isfile(os.path.join(root, f))]

    if show_summary is True:
        print(root)
        print("Iterating on %s files..." % (len(files)))

    files = list(filter(lambda f: f.split(
        '.')[-1] in supported_extensions, files))

    with concurrent.futures.ThreadPoolExecutor(None) as executor:
        for index, file in enumerate(files):
            executor.submit(lambda p: callback(*p), [file, root, index + 1])

        executor.shutdown()


def rename_to_exif(file, root, _):
    file_path = os.path.join(root, file)
    extension = file.split('.')[-1]
    tags = None

    with open(file_path, "rb") as f:
        try:
            tags = exifread.process_file(
                f, details=False, stop_tag="DateTimeOriginal")
        except Exception as e:
            print(e)
            print("Failed to get exif data", file_path)
            print('---')
        finally:
            f.close()

    if tags is not None:
        try:
            file_without_extension = file.replace("." + extension, "")
            new_name = f'{tags["EXIF DateTimeOriginal"]}_{file_without_extension}.'.replace(
                ":", "_")
            rename_file_group(root, file_path, new_name)
        except Exception as e:
            print(e)
            print("Failed to rename", file_path)
            print('---')


def rename_to_indexed(file, root, index):
    global prefix
    file_path = os.path.join(root, file)
    new_name = f'{prefix}{complete_image_id(str(index))}.'
    rename_file_group(root, file_path, new_name)


def rename_file_group(root, file_path, new_name):
    extension = file_path.split('.')[-1]

    for ext in [extension, CR2, CR3, XMP, AFPHOTO]:
        replaced_file_path = file_path.replace(
            extension, ext)

        if os.path.exists(replaced_file_path):
            os.rename(replaced_file_path, os.path.join(
                root, new_name + ext))
