from os import walk, path, rename

import exifread

path_to_images = input("Path to images: ")
category = input("Category: ")
date = input("Date: ")
# groups = input("Groups (3 5 4 6 3...): ").split(" ")

groups_s = '4 3 5 5 5 4 4 5 4 4 4 5 4 4 4 4 5 5 5 4 4 4 4 5 4 5 5 5 4 4 4 4 4 5 4 4 4 4 4 3 3 4 4 4 4 5 5 5 4 4 5 5 5 4 5 5 5 5 5 5 5 4 5 5 5 5 4 4 4 4 5 5 5 5 3 3 5 5 4 4 5 5 5 4 5 5 4 4 4 4 4 4 6 4 4 5 4 4 6 5 4 6 3 5 4 5 5 5 4 5 4 3 4 4 4 4 5 4 5 5 5 5 5 5 5 4 4 5 5 4 5 4 4 5 5 4 6 6 5 4 4 7 6 6 6 4 4 4 4 4 5 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 5 4 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 4 4 4 4 4 5 5 5 5 5 5 5 5 5 5 6 6 6 6 6 4 4 4 4 6 6 4 4 4 4 4 4 4 4 4 5 4 5 5 5 5 5 5 5 5 5 4 4 4 5 5 5 5 5 5 5 5 5 5 4 4 5 5 6 4 4 5 5 3 4 3 4 4 4 4 4 3 3 3 5 5 5 5 6 6 6 6 7 5 5 5 5 4 5 4 3 6 6 4 6 5 6 5 5 5 4 6 5 5 6 6 6 6 4 4 5 2 8 5 5 5 5 4 4 4 4 4 4 4 4 5 5 5 5 5 5 6 6 5 6 4 5 4 4 4 5 5 5 4 6 5 5 6 6 6 6 6 6 6 6 6 6 6 6 6 6 6 6 6 6 6 6 6 6 6 6 6 6 6 6 6 6 6 6 6 6 5 5 5 5 6 6 6 6 6 6 6 6 6 6 6 6 6 6 5'
groups = groups_s.split(" ")


# def convert_images():
#     path_to_images = "./resources"
#     i = 0
#
#     for root, dirs, files in walk(path_to_images):
#         src_root = root.replace("\\", "/") + "/"
#         target_root = "./output/" + (root + "\\").split("\\")[1]
#
#         if not path.exists(target_root):
#             makedirs(target_root)
#
#         for image in files:
#             src = src_root + image
#             target_image_root = target_root + "/"
#
#             if ".HEIC" in image:
#                 image_name = image.split(".")[0]
#
#                 subprocess.run(["magick", "%s" % src, "%s" % (target_image_root + image_name + '.jpg')])
#             elif ".JPG" in image:
#                 shutil.copy(src, target_image_root)
#
#             if i > 10:
#                 break
#
#             print("Converted " + image)


def rename_images():
    current_group_index = 0
    index_in_group = 1

    for root, dirs, files in walk(path_to_images):
        src_root = root.replace("\\", "/") + "/"

        for image in files:
            with open(path.join(root, image), "rb") as file:
                try:
                    tags = exifread.process_file(file)
                    file.close()

                    new_name = f'{tags["EXIF DateTimeOriginal"]}.jpg'.replace(":", "_")
                    rename(src_root + image, src_root + new_name)
                except:
                    print("Failed to get exif data", image)

    for root, dirs, files in walk(path_to_images):
        src_root = root.replace("\\", "/") + "/"

        for file in files:
            new_name = f'BB_{date}_Tesco_{category}_{int(current_group_index) + 1}_{index_in_group}.jpg'
            rename(src_root + file, src_root + new_name)

            index_in_group = index_in_group + 1

            if index_in_group > int(groups[current_group_index]):
                current_group_index = current_group_index + 1
                index_in_group = 1

            if current_group_index == len(groups):
                break


sum = 0

for n in groups:
    sum = sum + int(n)

rename_images()

print("Renamed " + str(sum) + " images!")
